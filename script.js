let main = function() {

    $('.btn').click(function(event) {     // Event listener on the "compose" button
        compose();
    });

    $('#toggle > span').click(function(){
        $('#canvas-art, #svg-art').toggle();
        $('#canvas-button, #svg-button').toggleClass('bold');
    });

    $(".show-more a").on("click", function() {  // Modified version of https://stackoverflow.com/a/12308064
        let $this = $(this); 
        let $content = $this.parent().prev("div.content");
        let linkText = $this.text();    
        
        linkText = (linkText === 'Show more ⮟' ? 'Show less ⮝' : 'Show more ⮟');
        $content.toggleClass('hideContent showContent');
        $this.text(linkText);
    });
};
$(document).ready(main);


function plussMinus() {
    return Math.round(Math.random()) * 2 - 1;
} 

// Generating a modular scale to be used in the "compose" phase. 
// All sizes used in the picture will have a common ratio of phi * n. Maybe it will make the pictures prettier -- who knows!
function fib(n){    // From https://medium.com/quick-code/fibonacci-sequence-javascript-interview-question-iterative-and-recursive-solutions-6a0346d24053
    let array = [1, 2];
    for (let i = 2; i < n + 1; i++){
      array.push(array[i - 2] + array[i -1])
    }
   return array[n]
}

const modularScaleBase = 32;
const modularScale = [...Array(6)].map((_, i) => fib(i) * modularScaleBase)   // Maps every entry in the array to a fibonacci number (times the modualr scale's base)
console.log(modularScale)

Array.prototype.randomElement = function () {   // Function for returning a random value from an array
    return this[~~(Math.random() * this.length)]
}

Array.prototype.last = function() {
    return this[this.length - 1]
}

function randomHSL(){   // Function for generating a "pretty" color. This is much easier with HSL than with RGB, since the saturation can be specified.
    return "hsl(" + ~~(360 * Math.random()) + "," +        // Random hue
                    (~~(10 * Math.random()) + 70) + "%," +   // Random saturation between 70 and 80 %
                    "75%)"                                // Lightness of colour. Could also be random, but I think it's nice as it is.
}

const vowels = ['a','e','ie','io','i','u','y','iu','ia']
const consonants = ['b','v','g','d','zj','z','j','k','l','m','n','o','p','r','s','t','f','kh','ts','tsj','sj']
const endings = ['iev', 'ovich', 'skij']

function artistName(){
    surenameLength = ~~(2 * Math.random()) + 3;
    lastnameLength = surenameLength + ~~(2 * Math.random)
    surename = '';
    lastname = '';

    for (let i = 0; i < surenameLength; i++) {
        if (i % 2 === 0) {
            surename += consonants.randomElement();
        }
        else {
            surename += vowels.randomElement();
        }
    }

    for (let j = 0; j < lastnameLength; j++) {
        if (j % 2 === 0) {
            lastname += consonants.randomElement();
        }
        else {
            lastname += vowels.randomElement();
        }
    }
    lastname += endings.randomElement();

    surename = surename.charAt(0).toUpperCase() + surename.slice(1);
    lastname = lastname.charAt(0).toUpperCase() + lastname.slice(1);

    return surename + ' ' + lastname
}


// The method of creating a simple, semi-random picture from scratch using squares, circles, lines etc.
function compose() {
    // Create callable variables for canvas and svg
    const canvas = $("#canvas-art")[0];
    const ctx = canvas.getContext("2d");
    const svg = $("#svg-art")[0];
    $("#artist-name").text(artistName());
    $("#painting-name").text("Composition Nº " + Math.round(Math.random()*10 + 1));

    ctx.clearRect(0, 0, canvas.width, canvas.height); // Clear canvas of shapes
    while (svg.firstChild){                         // Clear svg of shapes
        svg.removeChild(svg.firstChild);
    }

    //decide the number of squares, cricles, lines etc. to be put in the picture
    let squareNumber =  ~~(Math.random() * 3) + 1;
    let circleNumber = ~~(Math.random() * 2) + 1;
    let lineNumber = ~~(Math.random()) + 1;
    let curveNumber = ~~(Math.random()) + 1;

    drawBackground(canvas, ctx, svg);

    for (let i = 0; i <= squareNumber; i++){    //draw squares
        drawSquare(canvas, ctx, svg, i);
    }

    for (let j = 0; j <= circleNumber; j++){    //draw circles
        drawCircle(canvas, ctx, svg, j);
    }
    
    for (let k = 0; k <= lineNumber; k++){      //draw lines
        drawLine(canvas, ctx, svg, k);
    }

    for (let l = 0; l <= curveNumber; l++){      //draw curves
        drawCurve(canvas, ctx, svg, l);
    }
}


function drawBackground(canvas, ctx, svg) {     // Method of drawing a new, off-white background for every picture
    const x = 0;
    const y = 0;
    const h = canvas.height;
    const w = canvas.width;
    const lightness = (~~(Math.random() * 7) + 93);
    const color = "hsl(49, 55%, " + lightness + "%)";

    ctx.fillStyle = color;
    ctx.fillRect(x, y, w, h);

    const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    rect.setAttribute('id', 'backgorund');
    rect.setAttribute('class', 'svg-shape');
    rect.setAttribute('fill', color);
    rect.setAttribute('x', '' + x);
    rect.setAttribute('y', '' + y);
    rect.setAttribute('height', '' + h);
    rect.setAttribute('width', '' + w);
    svg.appendChild(rect);
}

function drawSquare(canvas, ctx, svg, i) {  //method of drawing the same square in canvas and svg
    //create random variables to define the shape
    const x = canvas.width * ~~(Math.random() * modularScaleBase)/modularScaleBase + modularScale.randomElement();
    const y = canvas.height * ~~(Math.random() * modularScaleBase)/modularScaleBase + modularScale.randomElement();
    const h = modularScale.randomElement();
    const w = modularScale.randomElement();
    const color = randomHSL();

    //draw canvas
    ctx.fillStyle = color;
    ctx.fillRect(x, y, w, h);

    //draw svg
    const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    rect.setAttribute('id', 'rect' + i);
    rect.setAttribute('class', 'svg-shape');
    rect.setAttribute('fill', color);
    rect.setAttribute('x', '' + x);
    rect.setAttribute('y', '' + y);
    rect.setAttribute('height', '' + h);
    rect.setAttribute('width', '' + w);
    svg.appendChild(rect);
}


function drawCircle(canvas, ctx, svg, j) {  //method of drawing the same circle in canvas and svg
    //create random variables to define the shape
    const x = canvas.width * ~~(Math.random() * modularScaleBase)/modularScaleBase + modularScale.randomElement();
    const y = canvas.height * ~~(Math.random() * modularScaleBase)/modularScaleBase + modularScale.randomElement();
    const radius = modularScale.randomElement();
    const color = randomHSL();

    //draw canvas
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
    ctx.strokeStyle = "rgba(1, 1, 1, 0)";
    ctx.stroke();
    ctx.fillStyle = color;
    ctx.fill();

    //draw svg
    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
    circle.setAttribute('id', 'circle' + j);
    circle.setAttribute('class', 'svg-shape');
    circle.setAttribute('fill', color);
    circle.setAttribute('cx', '' + x);
    circle.setAttribute('cy', '' + y);
    circle.setAttribute('r', '' + radius);
    circle.setAttribute('stroke', "rgba(1, 1, 1, 0)");
    svg.appendChild(circle);
}


function drawLine(canvas, ctx, svg, k) {  //method of drawing the same line in canvas and svg
    //create random variables to define the shape
    const x1 = canvas.width * ~~(Math.random() * modularScaleBase)/modularScaleBase + modularScale.randomElement();
    const y1 = canvas.height * ~~(Math.random() * modularScaleBase)/modularScaleBase + modularScale.randomElement();
    const x2 = x1 + plussMinus() * (modularScale.randomElement() + modularScale.last());
    const y2 = y1 + plussMinus() * (modularScale.randomElement() + modularScale.last());
    const color = Math.random() < 0.8 ? randomHSL() : "#1f1f1f";    // 20% chance of being black

    //draw canvas
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.strokeStyle = color;
    ctx.lineWidth = 3.5;
    ctx.stroke(); 

    //draw svg
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'line')
    line.setAttribute('id', 'line' + k);
    line.setAttribute('class', 'svg-shape');
    line.setAttribute('x1', '' + x1);
    line.setAttribute('y1', '' + y1);
    line.setAttribute('x2', '' + x2);
    line.setAttribute('y2', '' + y2);
    line.setAttribute('style', 'stroke:' + color + ';stroke-width:3.5')
    svg.appendChild(line);
}

function drawCurve(canvas, ctx, svg, l) {  //method of drawing the same curve in canvas and svg
    //create random variables to define the shape
    const x1 = canvas.width * ~~(Math.random() * modularScaleBase)/modularScaleBase + modularScale.randomElement();
    const y1 = canvas.height * ~~(Math.random() * modularScaleBase)/modularScaleBase + modularScale.randomElement();
    const x2 = x1 + plussMinus() * (modularScale.randomElement() + modularScale.last());
    const y2 = y1 + plussMinus() * (modularScale.randomElement() + modularScale.last());
    const q1 = (x1 + x2)/2 + plussMinus() * (modularScale.randomElement() + modularScale.last());
    const q2 = (y1 + y2)/2 + plussMinus() * (modularScale.randomElement() + modularScale.last());
    const color = Math.random() < 0.8 ? randomHSL() : "#1f1f1f";    // 20% chance of being black

    //draw canvas
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.quadraticCurveTo(q1, q2, x2, y2);
    ctx.strokeStyle = color;
    ctx.lineWidth = 3.5;
    ctx.stroke(); 

    //draw svg
    const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    path.setAttribute('id', 'path' + l);
    path.setAttribute('class', 'svg-shape');
    path.setAttribute('d', `M${x1},${y1} Q${q1},${q2} ${x2},${y2}`);
    path.setAttribute('style', 'stroke:' + color + ';stroke-width:3.5')
    path.setAttribute('fill', "rgba(1, 1, 1, 0)");
    svg.appendChild(path);
}